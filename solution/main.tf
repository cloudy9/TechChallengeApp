  
provider "aws" {
  region = "us-east-1"
}

variable "DBPASSWORD" {}

variable "DBUSER" {}

data "aws_vpc" "default" {
  default = true
}

data "aws_subnet_ids" "default" {
  vpc_id = data.aws_vpc.default.id
}

#Security Group for frontend ALB
resource "aws_security_group" "lb" {
  name        = "lb-sg"
  vpc_id = data.aws_vpc.default.id
  description = "controls access to the Application Load Balancer (ALB)"

  ingress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

}


#Creates Security Group for DB
resource "aws_security_group" "techapp-db" {
  name = "techapp-db"
  vpc_id = data.aws_vpc.default.id

  # Only postgres in
  ingress {
    from_port = 5432
    to_port = 5432
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Allow all outbound traffic.
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

#Creates Security Group for ALB to ECS
resource "aws_security_group" "ecs_tasks" {
  name        = "ecs-tasks-sg"
  vpc_id = data.aws_vpc.default.id
  description = "allow inbound access from the ALB only"

  ingress {
    protocol        = "tcp"
    from_port       = 3000
    to_port         = 3000
    #cidr_blocks     = ["0.0.0.0/0"]
    security_groups = [aws_security_group.lb.id]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  depends_on = [aws_security_group.lb]
}

#Creates AWS ALB
resource "aws_lb" "TechApp" {
  name               = "TechApp-alb"
  subnets            = data.aws_subnet_ids.default.ids
  load_balancer_type = "application"
  security_groups    = [aws_security_group.lb.id]

  depends_on = [aws_security_group.lb]
}

#Creates AWS ALB Listener
resource "aws_lb_listener" "http_forward" {
  load_balancer_arn = aws_lb.TechApp.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.TechApp.arn
  }
}

#Creates AWS ALB Target Group
resource "aws_lb_target_group" "TechApp" {
  name        = "techapp-alb-tg"
  port        = 3000
  protocol    = "HTTP"
  vpc_id      = data.aws_vpc.default.id
  target_type = "ip"

  health_check {
    enabled = true
    path    = "/healthcheck/"
    healthy_threshold   = "3"
    interval            = "30"
    protocol            = "HTTP"
    matcher             = "200"
    timeout             = "3"
    unhealthy_threshold = "2"
  }

  depends_on = [aws_lb.TechApp]
}



# Creates AWS Role with ECS Assume Role
resource "aws_iam_role" "techapp-task-execution-role" {
  name               = "techapp-task-execution-role"
  assume_role_policy = data.aws_iam_policy_document.ecs-task-assume-role.json
}

data "aws_iam_policy_document" "ecs-task-assume-role" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ecs-tasks.amazonaws.com"]
    }
  }
}


#Creates a ECS Task Execution Role with AWS Managed Policy
data "aws_iam_policy" "ecs-task-execution-role" {
  arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}

# Attach the above policy to the execution role.
resource "aws_iam_role_policy_attachment" "ecs-task-execution-role" {
  role       = aws_iam_role.techapp-task-execution-role.name
  policy_arn = data.aws_iam_policy.ecs-task-execution-role.arn

  depends_on = [aws_iam_role.techapp-task-execution-role]
}


#Creates AWS Cluster and builds the Docker Image
resource "aws_ecs_cluster" "techapp" {
    name = "TechApp"

  provisioner "local-exec" {
    working_dir = ".."
    command     = <<EOT
      docker build . -t techchallengeapp:latest
      docker tag techchallengeapp cloudy9/techchallengeapp:latest
      docker push cloudy9/techchallengeapp:latest
EOT
  }
}

#Creates AWS ECS Task Definition
resource "aws_ecs_task_definition" "techapp" {
    container_definitions = <<TASK_DEFINITION
[
    {
        "name": "techapp",
        "image": "cloudy9/techchallengeapp:latest",
        "cpu": 0,
        "portMappings": [
            {
                "containerPort": 3000,
                "hostPort": 3000,
                "protocol": "tcp"
            }
        ],
        "essential": true,
        "environment": [
            {
                "name": "VTT_DBUSER",
                "value": "${var.DBUSER}"
            },
            {
                "name": "VTT_DBPASSWORD",
                "value": "${var.DBPASSWORD}"
            },
            {
                "name": "VTT_DBHOST",
                "value": "${aws_db_instance.techapp.address}"
            },
            {
                "name": "VTT_DBNAME",
                "value": "${aws_db_instance.techapp.name }"
            },
            {
                "name": "VTT_DBPORT",
                "value": "5432"
            },
            {
                "name": "VTT_ListenHost",
                "value": "0.0.0.0"
            },
            {
                "name": "VTT_ListenPort",
                "value": "3000"
            }
        ],
        "mountPoints": [],
        "volumesFrom": [],
        "logConfiguration": {
            "logDriver": "awslogs",
            "options": {
                "awslogs-group": "/ecs/first-run-task-definition",
                "awslogs-region": "us-east-1",
                "awslogs-stream-prefix": "ecs"
            }
        }
    }
]
TASK_DEFINITION

    family = "techapp"
    task_role_arn = aws_iam_role.techapp-task-execution-role.arn
    execution_role_arn = aws_iam_role.techapp-task-execution-role.arn
    network_mode = "awsvpc"
    requires_compatibilities = [
        "FARGATE"
    ]
    cpu = "256"
    memory = "512"
}

resource "aws_cloudwatch_log_group" "ecs_log" {
  name = "/ecs/first-run-task-definition"
}

# Creates AWS ECS Service
resource "aws_ecs_service" "TechApp" {
  name            = "TechApp"
  cluster         = aws_ecs_cluster.techapp.id
  task_definition = aws_ecs_task_definition.techapp.arn
  desired_count   = 1
  deployment_minimum_healthy_percent = 50
  deployment_maximum_percent         = 200
  health_check_grace_period_seconds  = 60
  launch_type                        = "FARGATE"
  scheduling_strategy                = "REPLICA"

  network_configuration {
    security_groups  = [aws_security_group.ecs_tasks.id]
    subnets          = data.aws_subnet_ids.default.ids
    assign_public_ip = true
  }

  load_balancer {
    target_group_arn = aws_lb_target_group.TechApp.arn
    container_name   = "techapp"
    container_port   = 3000
  }

   depends_on = [aws_lb_target_group.TechApp, aws_lb_listener.http_forward, aws_iam_role_policy_attachment.ecs-task-execution-role]

   #lifecycle {
   # ignore_changes = [task_definition, desired_count]
  #}
}


#Creates RDS DB Subnet Group
resource "aws_db_subnet_group" "dbsubnet" {
  name       = "main2"
  subnet_ids = data.aws_subnet_ids.default.ids

  depends_on = [data.aws_subnet_ids.default]
}


#Deploys AWS RDS Postgres DB Instance
resource "aws_db_instance" "techapp" {
    name = "app"
    auto_minor_version_upgrade = "false"
    allow_major_version_upgrade = "false"
    copy_tags_to_snapshot = "true"
    multi_az = "false"
    publicly_accessible = "false"
    storage_encrypted = "false"
    allocated_storage = "20"
    backup_retention_period = "0"
    port = "5432"
    instance_class = "db.t2.micro"
    identifier = "app"
    parameter_group_name = "default.postgres10"
    db_subnet_group_name = aws_db_subnet_group.dbsubnet.name
    engine = "postgres"
    engine_version = "10.7"
    username = "postgres"
    password = var.DBPASSWORD
    option_group_name = "default:postgres-10"
    apply_immediately = "true"
    skip_final_snapshot = "true"
    storage_type = "gp2"
    vpc_security_group_ids = [aws_security_group.techapp-db.id]

    depends_on = [aws_security_group.techapp-db, aws_db_subnet_group.dbsubnet]

}


#Output DB hostname and ALB Address
output "db_host" {
  value = aws_db_instance.techapp.address
}

output "alb_address" {
  value = aws_lb.TechApp.dns_name
}


# Prerequisites

 This solution requires S3 Backed for Terraform State file to start with.

 # Solution

This solution has been deployed to empty AWS cloud.

1. Udpated Docker file to seed the data and start the application
2. Updated conf.toml to listen on '0.0.0.0'
3. Used GitLab CI/CD pieline for build and configuration
4. Given the simplicity requirement, used default vpc and subnets instead of putting the ECS behind the NAT Gateway
5. For security all the secrets have been protected and masked using Gitlab variables. Ideally AWS Secrets Manger or Parameter Store should be used.
6. Docker image stored within Docker Hub
7. Resiliency - ECS can be scaled if requied by changing the size


Application URL - http://TechApp-alb-1776190881.us-east-1.elb.amazonaws.com

TF Plan Job - https://gitlab.com/cloudy9/TechChallengeApp/-/jobs/747754338

TF Apply Job -  https://gitlab.com/cloudy9/TechChallengeApp/-/jobs/747754339

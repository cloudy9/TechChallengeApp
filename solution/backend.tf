terraform {
    required_version = ">= 0.12.16"
    backend "s3" {
    bucket         = "tfstate1212"
    key            = "techapp1/techapp1_aws.tfstate"
    region         = "us-east-1"
    encrypt        = true
  }
}


